#!/bin/bash

crontab $1 > /dev/null 2>&1

if [ "$?" -ne 0 ]
then
echo "No"
exit
else
echo "Yes"
fi
