#!/bin/bash
task=$1
if [ $task = "write" ]
then
	echo "$2,$3,$4" >> employee.txt
	tail -n +2 employee.txt | sort | tee employee.txt | sed -i '1s/^/eno,ename,salary\n/' employee.txt
	echo Done
	exit
fi
if [ $task = "read" ]
then
	if [ "$2" = "eno" ]
	then
		cat employee.txt | grep "^"$3","
		exit
	fi
	if [ "$2" = "ename" ]
	then
		cat employee.txt | grep "^"$3","
	fi
	if [ "$2" = "salary" ]
	then
		cat employee.txt | grep "\b,"$3"\b"
	fi
fi
if [ $task = "delete" ]
then
	cat employee.txt | grep -v "^"$2"," | tee employee.txt > /dev/null 
	echo Done 
	exit
fi
if [ $task = "update" ]
then
	cat employee.txt | grep -v "^"$2"," | tee employee.txt > /dev/null
	echo "$2,$3,$4" >> employee.txt
	tail -n +2 employee.txt | sort | tee employee.txt | sed -i '1s/^/eno,ename,salary\n/' employee.txt
	echo Done
	exit
fi
if [ $task = "duplicate" ]
then
	q=`cat employee.txt | uniq -c | tr -s ' ' | grep -v "1" | cut -d ' ' -f3`
	echo $q
	exit
fi
if [ $task = "nthsalary" ]
then
	p=`cat employee.txt | tr ',' ' ' | cat | sort -k3 -n -r | uniq -c -f2 | tr -s " " | cat | head -n $2 | tail -n 1 | awk '{print $4}'`
	grep "\b,$p\b" employee.txt | cat
fi
