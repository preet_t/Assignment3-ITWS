#!/bin/bash

n=$#
n=$((n-1))
p=$1
arr=("$@")

for ((i=1; i<=$n; i++))
do
	p=$((p**${arr[$i]}))
done

echo $p
