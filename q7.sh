#!/bin/bash

read string
d=`echo "$string" | tr '[:upper:]' '[:lower:]'`
e=`echo "$d" | rev`
if [ "$d" = "$e" ]
then
	echo Yes
else
	echo No
fi
