#!/bin/bash
read operator
read n
declare -a store
for((i=1; i<=$n; i++))
do
	read store[$i-1]
done
p=${store[0]}
for ((i=1; i<=$((n-1)); i++))
do
	p=`echo "scale = 4; $p $operator ${store[$i]}" | bc`
done
printf "%.4f" $p
echo
