#!/bin/bash
HISTFILE=~/.bash_history
set -o history
history | tail ...
history | awk '{print $2}' | sort | uniq -c | sort -nr | awk '{print $2 ":" $1}'

