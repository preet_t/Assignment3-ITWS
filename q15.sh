#!/bin/bash
elinks -dump $1 > $3 
elinks -dump $2 >> $3
cat $3 | tr '[:punct:]' ' ' | tr ' ' '\n' | grep . | cat | sort | uniq -c | tr -s ' ' | awk '{print $2" "$1}' | sort -k2nr -k1 > $3
cat $3
