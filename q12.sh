#!/bin/bash

read l

m=`echo $l | cut -d ' ' -f1`
d=`echo $l | cut -d ',' -f1 | cut -d ' ' -f2`
y=`echo $l | cut -d ' ' -f3`

if [[ $m = january ]] || [[ $m = jan ]]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=01
fi

if [[ $m = february ]] || [[ $m = feb ]] 
then
	if [ $((y%4)) -ne 0 ] 
	then 
		if [ $d -ge 29 ] || [ $d -lt 1 ]
		then
		echo Invalid
		exit
		fi	
	fi

	if [ $((y%4)) -eq 0 ]
	then
		if [ $d -gt 29 ] || [ $d -lt 1 ]
		then
		echo Invalid
		exit
		fi
	fi

	m=02	
fi

if [[ $m = march ]] || [[ $m = mar ]]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=03
fi

if [[ $m = april ]] || [[ $m = apr ]]
then
	if [ $d -gt 30 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=04
fi
if [[ $m = may ]] 

then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=05
fi

if [[ $m = june ]] || [[ $m = jun ]]
then
	if [ $d -gt 30 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=06
fi

if [[ $m = july ]] || [[ $m = jul ]]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=07
fi

if [[ $m = august ]] || [[ $m = aug ]]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=08
fi

if [[ $m = september ]] || [[ $m = sept ]]
then
	if [ $d -gt 30 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=09
fi

if [[ $m = october ]] || [[ $m = oct ]]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=10
fi

if [[ $m = november ]] || [[ $m = nov ]]
then
	if [ $d -gt 30 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=11
fi

if [[ $m = december ]] || [[ $m = dec ]]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo Invalid
		exit
	fi
	m=12
fi

echo "Choose The Number Corresponding To The Format In Which You Want To Get The Date"

echo

echo "MM/DD/YYYY    1"
echo "DD-MM-YYYY    2"

echo

read -p 'Enter The Number:' pappu

if [ $pappu -eq 1 ]
then 
	echo "$m/$d/$y"
else
	echo "$d-$m-$y"
fi
echo
