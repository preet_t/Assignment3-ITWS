#!/bin/bash

touch q14.html
chmod 755 q14.html
pwd=`echo $PWD`

echo "<!DOCTYPE html> 
<html> 
<body>
<table style="width:100%" border="2">" >> q14.html
		echo "<tr>
		<th colspan="3">$PWD</th>
		</tr>
		<tr>
		<th>Name</th>
		<th>Size</th>
		<th>Type</th>
		</tr>" >> q14.html
	
	du ./ -ha -d1 | 
	
	while read pop; 
	do	        
		name=`echo "$pop" | cut -d "/" -f2`
		size=`echo "$pop" | awk '{print $1}'`
		
		if [ -d "$name" ]
		then 
			t=directory
		else
			t=file
		fi

		echo "<tr>
		<td align="center">$name</td>
		<td align="center">$size</td>
		<td align="center">$t</td>
		</tr>" >> q14.html
	done

	du ./ -h -d2 | cut -d "/" -f2 | grep . | cat | sort -n | uniq | 

	while read opo;
	do
		echo "<tr>
		<th colspan="3">$opo</th>
		</tr>
		<tr>
		<th>Name</th>
		<th>Size</th>
		<th>Type</th>
		</tr>" >> q14.html
	
		ls -l $opo | tail -n +2 |  
		while read var; 
		do	        
			t=`echo $var | head -c 1`
			if [ "$t" = "d" ]
			then 
				t=directory
			else
				t=file
			fi
			if [ "$t" = t ]
			then 
				continue
			fi
			
			name=`echo "$var" | awk '{print $9}'`
			size=`du ./$opo/$name -h | tail -n 1 | awk '{print $1}'` 
			
			echo "<tr>
			<td align="center">$name</td>
			<td align="center">$size</td>
			<td align="center">$t</td>
			</tr>" >> q14.html
		done
	done

echo "</table>
</body>
</html>" >> q14.html
firefox q14.html 
